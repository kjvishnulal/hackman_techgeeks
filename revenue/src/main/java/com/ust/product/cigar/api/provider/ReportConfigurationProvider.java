package com.ust.product.cigar.api.provider;

import net.sf.jasperreports.export.ReportExportConfiguration;

public interface ReportConfigurationProvider<Configuration extends ReportExportConfiguration> {

	Configuration configuration();
}
