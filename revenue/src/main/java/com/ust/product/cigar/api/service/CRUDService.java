package com.ust.product.cigar.api.service;

import java.io.Serializable;
import java.util.Collection;

public interface CRUDService<Model,PrimaryKey extends Serializable> {

	Model save(Model model);
	void delete(Model model);
	void deleteByPirmaryKey(PrimaryKey pk);
	Collection<Model> findAll();
	Model findOne(PrimaryKey pk);
	Model update(Model model);
}
