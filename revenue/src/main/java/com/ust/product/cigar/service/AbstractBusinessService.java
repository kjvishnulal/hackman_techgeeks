package com.ust.product.cigar.service;

import java.io.Serializable;

import javax.validation.Valid;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ust.product.cigar.service.properties.ServiceProperties;

public abstract class AbstractBusinessService<Model, PrimaryKey extends Serializable> extends AbstractCRUDService<Model, PrimaryKey> {

	
	@Autowired
	protected Mapper beanMapper;
	

	protected final ServiceProperties serviceProperties;
	
	protected AbstractBusinessService(@Valid ServiceProperties serviceProperties, JpaRepository<Model, PrimaryKey> repository) {
		super(repository);
		this.serviceProperties = serviceProperties;
	}
}
