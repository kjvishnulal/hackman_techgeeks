package com.ust.product.cigar.model.report;

import java.util.Collection;

public final class GenericForecastReport extends AbstractJasperReport {

	private Collection<LocationWiseGenericForecastReportModule> locationWiseGenericForecastReportModules;

	public Collection<LocationWiseGenericForecastReportModule> getLocationWiseGenericForecastReportModules() {
		return locationWiseGenericForecastReportModules;
	}

	public void setLocationWiseGenericForecastReportModules(
			Collection<LocationWiseGenericForecastReportModule> locationWiseGenericForecastReportModules) {
		this.locationWiseGenericForecastReportModules = locationWiseGenericForecastReportModules;
	}


}
