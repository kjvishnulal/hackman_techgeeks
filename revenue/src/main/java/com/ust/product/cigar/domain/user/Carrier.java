package com.ust.product.cigar.domain.user;

import java.util.Collection;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.ust.product.cigar.domain.transport.Vehicle;

@Entity
@DiscriminatorValue("2")
public class Carrier extends AbstractUser {

	@OneToMany
	private Collection<Vehicle> vehicles;
	
	
	
}
