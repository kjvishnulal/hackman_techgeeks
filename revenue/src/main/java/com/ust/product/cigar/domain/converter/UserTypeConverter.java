package com.ust.product.cigar.domain.converter;

import javax.persistence.AttributeConverter;

import com.ust.product.cigar.domain.user.UserType;

public class UserTypeConverter implements AttributeConverter<UserType, String> {

	@Override
	public String convertToDatabaseColumn(UserType userType) {
		return userType.getUserType();
	}

	@Override
	public UserType convertToEntityAttribute(String userType) {
		return UserType.resolveuserType(userType);
	}

}
