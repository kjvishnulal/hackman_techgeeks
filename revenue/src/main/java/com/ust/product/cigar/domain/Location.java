package com.ust.product.cigar.domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_LOCATION")
public class Location extends AbstractPersistable {
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "CODE_ID")
	private Code code;
}
