package com.ust.product.cigar.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_CODE_DESC")
public class CodeDescription extends AbstractPersistable {

	@ManyToOne
	private Code code;


	@Column(name = "description")
	private String description;

	public Code getCode() {
		return code;
	}

	public void setCode(Code code) {
		this.code = code;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
