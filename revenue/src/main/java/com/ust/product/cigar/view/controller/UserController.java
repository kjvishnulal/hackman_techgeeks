package com.ust.product.cigar.view.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;

import com.ust.product.cigar.util.constants.BeanIdentifier;
import com.ust.product.cigar.view.common.properties.WebUIProperties;

public abstract class UserController<T extends WebUIProperties> extends AbstractViewController<T> {

	@Autowired
	public UserController(T uiProperties) {
		super(uiProperties);
	}

	protected void doOnPostConstruction() {
	}

	
	@GetMapping("home")
	public abstract String navigateToLandingPage();

}
