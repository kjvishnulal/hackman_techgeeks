package com.ust.product.cigar.service.properties;

import javax.validation.constraints.NotNull;


public class ProjectProperties {

	@NotNull
	private Integer accountCode;

	public Integer getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(Integer accountCode) {
		this.accountCode = accountCode;
	}
	
	
}
