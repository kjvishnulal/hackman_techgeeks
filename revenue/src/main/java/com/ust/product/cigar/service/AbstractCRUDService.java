package com.ust.product.cigar.service;

import java.io.Serializable;
import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ust.product.cigar.api.service.CRUDService;
import com.ust.product.cigar.logger.AbstractApplicationLogger;

public class AbstractCRUDService<Model, PrimaryKey extends Serializable> extends AbstractApplicationLogger implements CRUDService<Model, PrimaryKey>  {

	protected final JpaRepository<Model, PrimaryKey> repository;
	
	public AbstractCRUDService(JpaRepository<Model, PrimaryKey> repository) {
		this.repository = repository;
	}

	@Override
	public Model save(Model model) {
		return repository.save(model);
	}

	@Override
	public void delete(Model model) {
		repository.delete(model);
	}

	@Override
	public void deleteByPirmaryKey(PrimaryKey pk) {
		repository.delete(pk);
	}

	@Override
	public Collection<Model> findAll() {
		return repository.findAll();
	}

	@Override
	public Model findOne(PrimaryKey pk) {
		return repository.findOne(pk);
	}

	@Override
	public Model update(Model model) {
		return repository.save(model);
	}

}
