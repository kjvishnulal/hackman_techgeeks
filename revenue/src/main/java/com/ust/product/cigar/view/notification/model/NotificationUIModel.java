package com.ust.product.cigar.view.notification.model;

import com.ust.product.cigar.api.model.UIModel;

public class NotificationUIModel implements UIModel {

	private static final long serialVersionUID = 2502300615974565901L;

	private Long driverId;
	private Long vehicleId;
	private Long consumerId;
	private Long notificationId;
	private Long rate;

	public Long getDriverId() {
		return driverId;
	}

	public void setDriverId(Long driverId) {
		this.driverId = driverId;
	}

	public Long getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(Long vehicleId) {
		this.vehicleId = vehicleId;
	}

	public Long getConsumerId() {
		return consumerId;
	}

	public void setConsumerId(Long consumerId) {
		this.consumerId = consumerId;
	}

	public Long getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(Long notificationId) {
		this.notificationId = notificationId;
	}

	public Long getRate() {
		return rate;
	}

	public void setRate(Long rate) {
		this.rate = rate;
	}

}
