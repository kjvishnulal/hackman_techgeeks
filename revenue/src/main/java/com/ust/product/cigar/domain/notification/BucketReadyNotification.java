package com.ust.product.cigar.domain.notification;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ust.product.cigar.domain.AbstractPersistable;
import com.ust.product.cigar.domain.user.Consumer;

@Entity
@Table(name="TBL_BUCKET_READY_NOTIFICATION")
public class BucketReadyNotification extends AbstractPersistable {

	@ManyToOne
	@JoinColumn(name = "CONSUMER_ID", referencedColumnName = "ID")
	private Consumer consumer;

	@Column(name = "DESTINATION")
	private String destination;

	public Consumer getConsumer() {
		return consumer;
	}

	public void setConsumer(Consumer consumer) {
		this.consumer = consumer;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}
}
