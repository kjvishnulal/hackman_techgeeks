package com.ust.product.cigar.view.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ust.product.cigar.api.consumer.service.ConsumerService;
import com.ust.product.cigar.util.constants.BeanIdentifier;
import com.ust.product.cigar.view.vendor.model.VendorUIModel;
import com.ust.product.cigar.view.vendor.properties.VendorUIProperties;

@Controller(BeanIdentifier.Vendor.UI_CONTROLLER_IDENTIFIER)
@RequestMapping("/vendor")
public class VendorController extends UserController<VendorUIProperties> {

	protected final static String VIEW_CRITERIA_MODEL = "consumer-view-criteria";
	protected final static String VIEW_CONSUMER_MODEL = "consumer-view-model";

@Autowired
private ConsumerService consumerService;
	@Autowired
	public VendorController(
			@Qualifier(BeanIdentifier.Vendor.UI_PROPERTIES_IDENTIFIER) VendorUIProperties uiProperties) {
		super(uiProperties);
	}

	@Override
	protected void doOnPostConstruction() {
	}


	@ModelAttribute(name = VIEW_CONSUMER_MODEL)
	protected VendorUIModel initializeUIModel() {
		return new VendorUIModel();
	}
	
	public String navigateToLandingPage() {
		return ((VendorUIProperties)uiProperties).getNavigation().getLandingPage();
	}
	
	@PostMapping("register")
	public VendorUIModel save(@ModelAttribute(name = VIEW_CONSUMER_MODEL) VendorUIModel consumerUIRecord) {
		return consumerUIRecord;
	}
	
}
