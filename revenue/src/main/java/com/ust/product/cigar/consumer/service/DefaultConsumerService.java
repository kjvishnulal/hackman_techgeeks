package com.ust.product.cigar.consumer.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ust.product.cigar.api.consumer.service.ConsumerService;
import com.ust.product.cigar.api.repository.ConsumerRepository;
import com.ust.product.cigar.domain.address.Address;
import com.ust.product.cigar.domain.contact.Contact;
import com.ust.product.cigar.domain.user.Consumer;
import com.ust.product.cigar.domain.user.UserType;
import com.ust.product.cigar.service.AbstractBusinessService;
import com.ust.product.cigar.service.properties.ServiceProperties;
import com.ust.product.cigar.view.address.model.AddressUIModel;
import com.ust.product.cigar.view.consumer.model.ConsumerUIModel;
import com.ust.product.cigar.view.contact.model.ContactUIModel;

@Service
public class DefaultConsumerService extends AbstractBusinessService<Consumer, Long> implements ConsumerService {

	protected DefaultConsumerService(@Autowired ServiceProperties serviceProperties,
			@Autowired ConsumerRepository consumerRepository) {
		super(serviceProperties, consumerRepository);
	}

	@Override
	public void save(ConsumerUIModel consumerUIRecord) {
		final Consumer consumer = new Consumer();
		consumer.setName(consumerUIRecord.getName());
		consumer.setUserType(UserType.PUBLIC);
		
		consumer.setAddresses(new ArrayList<Address>());
		for (final AddressUIModel addressUiModel : consumerUIRecord.getAddresses()) {
			final Address address = Address.toAddress(addressUiModel, new Address());
			consumer.getAddresses().add(address);
		}

		consumer.setContacts(new ArrayList<Contact>());
		for (final ContactUIModel contactUIModel : consumerUIRecord.getContacts()) {
			final Contact contact = Contact.toContact(contactUIModel, new Contact());
			consumer.getContacts().add(contact);
		}
		save(consumer);
	}

}
