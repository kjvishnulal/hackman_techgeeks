package com.ust.product.cigar.carrier.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ust.product.cigar.api.carrier.service.CarrierService;
import com.ust.product.cigar.api.repository.CarrierRepository;
import com.ust.product.cigar.domain.address.Address;
import com.ust.product.cigar.domain.contact.Contact;
import com.ust.product.cigar.domain.user.Carrier;
import com.ust.product.cigar.domain.user.UserType;
import com.ust.product.cigar.service.AbstractBusinessService;
import com.ust.product.cigar.service.properties.ServiceProperties;
import com.ust.product.cigar.view.address.model.AddressUIModel;
import com.ust.product.cigar.view.carrier.model.CarrierUIModel;
import com.ust.product.cigar.view.contact.model.ContactUIModel;

@Service
public class DefaultCarrierService extends AbstractBusinessService<Carrier, Long> implements CarrierService {

	protected DefaultCarrierService(@Autowired ServiceProperties serviceProperties,
			@Autowired CarrierRepository carrierRepository) {
		super(serviceProperties, carrierRepository);
	}

	@Override
	public void save(CarrierUIModel carrierUIRecord) {
		final Carrier carrier = new Carrier();
		carrier.setName(carrierUIRecord.getName());
		carrier.setUserType(UserType.CARRIER);
		
		carrier.setAddresses(new ArrayList<Address>());
		for (final AddressUIModel addressUiModel : carrierUIRecord.getAddresses()) {
			final Address address = Address.toAddress(addressUiModel, new Address());
			carrier.getAddresses().add(address);
		}

		carrier.setContacts(new ArrayList<Contact>());
		for (final ContactUIModel contactUIModel : carrierUIRecord.getContacts()) {
			final Contact contact = Contact.toContact(contactUIModel, new Contact());
			carrier.getContacts().add(contact);
		}
		save(carrier);
	}

}
