package com.ust.product.cigar.api.bid.service;

import com.ust.product.cigar.api.service.CRUDService;
import com.ust.product.cigar.domain.transport.ConsumerTransport;
import com.ust.product.cigar.view.bid.model.BidUIModel;
import com.ust.product.cigar.view.consumer.model.ConsumerUIModel;
import com.ust.product.cigar.view.notification.model.NotificationUIModel;

public interface BidService extends CRUDService<ConsumerTransport, Long> {

	void save(BidUIModel bidUIModel);
	
	NotificationUIModel createNotification(ConsumerUIModel consumerUIModel, String destination);
	
	BidUIModel makeABid(final BidUIModel bidUIModel);
}
