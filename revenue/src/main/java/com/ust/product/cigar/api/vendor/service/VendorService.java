package com.ust.product.cigar.api.vendor.service;

import com.ust.product.cigar.api.service.CRUDService;
import com.ust.product.cigar.domain.user.Vendor;
import com.ust.product.cigar.view.vendor.model.VendorUIModel;

public interface VendorService extends CRUDService<Vendor, Long> {

	void save(VendorUIModel vendorUIModel);
}
