package com.ust.product.cigar.domain.user;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.ust.product.cigar.domain.AbstractPersistable;
import com.ust.product.cigar.domain.address.Address;
import com.ust.product.cigar.domain.contact.Contact;
import com.ust.product.cigar.domain.converter.UserTypeConverter;

@Entity
@Table(name="TBL_USER")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class AbstractUser extends AbstractPersistable{

	@Column(name="User_Type")
	@Enumerated
	@Convert(converter= UserTypeConverter.class)
	private UserType userType;
	
	private String name;
	
	@OneToMany
	private Collection<Address> addresses;
	
	@OneToMany
	private Collection<Contact> contacts;

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Collection<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(Collection<Address> addresses) {
		this.addresses = addresses;
	}

	public Collection<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(Collection<Contact> contacts) {
		this.contacts = contacts;
	}
}
