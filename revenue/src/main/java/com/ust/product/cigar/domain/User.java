package com.ust.product.cigar.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="TBL_USER")
public class User extends AbstractPersistable {

	@Column(name="NAME")
	private String name;
	
}
