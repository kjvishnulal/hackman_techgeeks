package com.ust.product.cigar.view.common.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.ust.product.cigar.util.constants.BeanIdentifier;

@Component(BeanIdentifier.Application.UI_PROPERTIES_IDENTIFIER)
@ConfigurationProperties(prefix = "com.ust.revenue.application.view")
public class CommonViewProperties {

	private String errorUrl;
	private String urlPrefix;
	
	public String getErrorUrl() {
		return errorUrl;
	}
	public void setErrorUrl(String errorUrl) {
		this.errorUrl = errorUrl;
	}
	public String getUrlPrefix() {
		return urlPrefix;
	}
	public void setUrlPrefix(String urlPrefix) {
		this.urlPrefix = urlPrefix;
	}
	
	
}
