package com.ust.product.cigar.api.builder;

import net.sf.jasperreports.engine.JRDataSource;

public interface JasperDatasourceBuilder<T> extends DatasourceBuilder {

	JRDataSource createDatasource(T model);
}
