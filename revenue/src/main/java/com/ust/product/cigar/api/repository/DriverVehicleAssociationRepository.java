package com.ust.product.cigar.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ust.product.cigar.domain.transport.DriverMappedToVehiclePrimaryKey;
import com.ust.product.cigar.domain.transport.DriverVehicleAssociation;

@Repository
public interface DriverVehicleAssociationRepository extends JpaRepository<DriverVehicleAssociation, DriverMappedToVehiclePrimaryKey> {

}
