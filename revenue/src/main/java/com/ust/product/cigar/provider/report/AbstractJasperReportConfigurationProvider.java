package com.ust.product.cigar.provider.report;

import com.ust.product.cigar.api.provider.ReportConfigurationProvider;

import net.sf.jasperreports.export.ReportExportConfiguration;

public abstract class AbstractJasperReportConfigurationProvider<Configuration extends ReportExportConfiguration> implements ReportConfigurationProvider<ReportExportConfiguration> {

}
