package com.ust.product.cigar.domain.address;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.ust.product.cigar.domain.AbstractPersistable;
import com.ust.product.cigar.view.address.model.AddressUIModel;

@Entity
@Table(name = "TBL_ADDRESS")
public class Address extends AbstractPersistable {

	@Column(name = "HOUSE_NAME")
	private String houseName;

	@Column(name = "STREET")
	private String street;

	@Column(name = "NEAR_BYE")
	private String nearBy;

	@Column(name = "PIN")
	private Integer pin;

	public String getHouseName() {
		return houseName;
	}

	public void setHouseName(String houseName) {
		this.houseName = houseName;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getNearBy() {
		return nearBy;
	}

	public void setNearBy(String nearBy) {
		this.nearBy = nearBy;
	}

	public Integer getPin() {
		return pin;
	}

	public void setPin(Integer pin) {
		this.pin = pin;
	}

	public static Address toAddress(AddressUIModel addressUiModel, Address address) {
		address.houseName = addressUiModel.getHouseName();
		address.nearBy = addressUiModel.getNearBy();
		address.street = addressUiModel.getStreet();
		address.pin = addressUiModel.getPin();
		return address;
	}
}
