package com.ust.product.cigar.view.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ust.product.cigar.api.consumer.service.ConsumerService;
import com.ust.product.cigar.util.constants.BeanIdentifier;
import com.ust.product.cigar.view.consumer.model.ConsumerUIModel;
import com.ust.product.cigar.view.consumer.properties.ConsumerUIProperties;

@Controller(BeanIdentifier.Consumer.UI_CONTROLLER_IDENTIFIER)
@RequestMapping("/consumer")
public class ConsumerController extends UserController<ConsumerUIProperties> {

	protected final static String VIEW_CRITERIA_MODEL = "consumer-view-criteria";
	protected final static String VIEW_CONSUMER_MODEL = "consumer-view-model";

@Autowired
private ConsumerService consumerService;
	@Autowired
	public ConsumerController(
			@Qualifier(BeanIdentifier.Consumer.UI_PROPERTIES_IDENTIFIER) ConsumerUIProperties uiProperties) {
		super(uiProperties);
	}

	@Override
	protected void doOnPostConstruction() {
	}


	@ModelAttribute(name = VIEW_CONSUMER_MODEL)
	protected ConsumerUIModel initializeUIModel() {
		return new ConsumerUIModel();
	}
	
	public String navigateToLandingPage() {
		return ((ConsumerUIProperties)uiProperties).getNavigation().getLandingPage();
	}
	
	@PostMapping("register")
	public ConsumerUIModel save(@ModelAttribute(name = VIEW_CONSUMER_MODEL) ConsumerUIModel consumerUIRecord) {
		consumerService.save(consumerUIRecord);
		return consumerUIRecord;
	}
	
}
