package com.ust.product.cigar.view.consumer.criteria;

import javax.validation.constraints.NotNull;

import com.ust.product.cigar.api.criteria.UICriteria;


public class ProjectUICriteria implements UICriteria {

	private static final long serialVersionUID = 2502300615974565901L;

	@NotNull
	protected String name;

	protected Long id;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	
}
