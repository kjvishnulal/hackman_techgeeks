package com.ust.product.cigar.api.exporter;

public interface ForecastReportUIExporter<Model> {

	void export(Model model);
}
