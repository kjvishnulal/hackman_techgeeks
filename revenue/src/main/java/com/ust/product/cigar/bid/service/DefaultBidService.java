package com.ust.product.cigar.bid.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ust.product.cigar.api.bid.service.BidService;
import com.ust.product.cigar.api.repository.BucketReadyNotificationRepository;
import com.ust.product.cigar.api.repository.ConsumerRepository;
import com.ust.product.cigar.api.repository.ConsumerTransportRepository;
import com.ust.product.cigar.api.repository.DriverVehicleAssociationRepository;
import com.ust.product.cigar.domain.notification.BucketReadyNotification;
import com.ust.product.cigar.domain.transport.ConsumerTransport;
import com.ust.product.cigar.domain.transport.DriverMappedToVehiclePrimaryKey;
import com.ust.product.cigar.domain.transport.DriverVehicleAssociation;
import com.ust.product.cigar.domain.user.Consumer;
import com.ust.product.cigar.service.AbstractBusinessService;
import com.ust.product.cigar.service.properties.ServiceProperties;
import com.ust.product.cigar.view.bid.model.BidUIModel;
import com.ust.product.cigar.view.consumer.model.ConsumerUIModel;
import com.ust.product.cigar.view.notification.model.NotificationUIModel;

@Service
public class DefaultBidService extends AbstractBusinessService<ConsumerTransport, Long> implements BidService {

	@Autowired
	private ConsumerRepository consumerRepository;
	
	@Autowired
	private BucketReadyNotificationRepository bucketReadyNotificationRepository;
	
	@Autowired
	private ConsumerTransportRepository consumerTransportRepository;
	
	@Autowired
	private DriverVehicleAssociationRepository driverVehicleAssociationRepository;
	
	protected DefaultBidService(@Autowired ServiceProperties serviceProperties,
			@Autowired ConsumerTransportRepository consumerTransportRepository) {
		super(serviceProperties, consumerTransportRepository);
	}

	@Override
	public void save(BidUIModel bidUIModel) {

	}

	@Override
	public NotificationUIModel createNotification(ConsumerUIModel consumerUIModel, String destination) {
		final Consumer consumer = consumerRepository.findOne(consumerUIModel.getId());
		final BucketReadyNotification notification = new BucketReadyNotification();
		notification.setConsumer(consumer);
		notification.setDestination(destination);
		bucketReadyNotificationRepository.save(notification);
		
		final NotificationUIModel notifUIModel = new NotificationUIModel();
		notifUIModel.setConsumerId(consumerUIModel.getId());
		notifUIModel.setNotificationId(notification.getId());
		return notifUIModel;
	}

	@Override
	public BidUIModel makeABid(BidUIModel bidUIModel) {
		final Consumer consumer = consumerRepository.findOne(bidUIModel.getConsumerId());
		
		final ConsumerTransport consumerTransport = new ConsumerTransport();
		final DriverMappedToVehiclePrimaryKey pk = new DriverMappedToVehiclePrimaryKey();
		pk.setDriverId(bidUIModel.getDriverId());
		pk.setVehicleId(bidUIModel.getVehicleId());;
		final DriverVehicleAssociation  driverVehicleAssociation = driverVehicleAssociationRepository.findOne(pk);
		consumerTransport.setConsumer(consumer);
		consumerTransport.setDriverVehicleAssociation(driverVehicleAssociation);
		consumerTransport.setRate(bidUIModel.getRate());
		
		consumerTransportRepository.save(consumerTransport);
		
		bidUIModel.setId(consumerTransport.getId());
		return bidUIModel;
	}
	

}
