package com.ust.product.cigar.view.common.properties;

import org.hibernate.validator.constraints.NotEmpty;

public class NavigationProperties {

	@NotEmpty
	private String landingPage;
	private String urlPrefix;
	
	public String getLandingPage() {
		return landingPage;
	}

	public void setLandingPage(String landingPage) {
		this.landingPage = landingPage;
	}

	public String getUrlPrefix() {
		return urlPrefix;
	}

	public void setUrlPrefix(String urlPrefix) {
		this.urlPrefix = urlPrefix;
	}
	
	
}
