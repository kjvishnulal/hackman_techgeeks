package com.ust.product.cigar.api.wastemgmt.service;

import com.ust.product.cigar.api.service.CRUDService;
import com.ust.product.cigar.domain.user.WasteManagementGroup;
import com.ust.product.cigar.view.wastemanagement.model.WasteManagementUIModel;

public interface WasteManagementGroupService extends CRUDService<WasteManagementGroup, Long> {

	void save(WasteManagementUIModel managementUIModel);
}
