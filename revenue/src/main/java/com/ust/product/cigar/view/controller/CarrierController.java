package com.ust.product.cigar.view.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ust.product.cigar.api.carrier.service.CarrierService;
import com.ust.product.cigar.util.constants.BeanIdentifier;
import com.ust.product.cigar.view.carrier.model.CarrierUIModel;
import com.ust.product.cigar.view.carrier.properties.CarrierUIProperties;

@Controller(BeanIdentifier.Carrier.UI_CONTROLLER_IDENTIFIER)
@RequestMapping("/carrier")
public class CarrierController extends UserController<CarrierUIProperties> {

	protected final static String VIEW_CRITERIA_MODEL = "consumer-view-criteria";
	protected final static String VIEW_CONSUMER_MODEL = "consumer-view-model";

@Autowired
private CarrierService carrierService;
	@Autowired
	public CarrierController(
			@Qualifier(BeanIdentifier.Carrier.UI_PROPERTIES_IDENTIFIER) CarrierUIProperties uiProperties) {
		super(uiProperties);
	}

	@Override
	protected void doOnPostConstruction() {
	}


	@ModelAttribute(name = VIEW_CONSUMER_MODEL)
	protected CarrierUIModel initializeUIModel() {
		return new CarrierUIModel();
	}
	
	public String navigateToLandingPage() {
		return ((CarrierUIProperties)uiProperties).getNavigation().getLandingPage();
	}
	
	@PostMapping("register")
	public CarrierUIModel save(@ModelAttribute(name = VIEW_CONSUMER_MODEL) CarrierUIModel carrierUIModel) {
		carrierService.save(carrierUIModel);
		return carrierUIModel;
	}
	
}
