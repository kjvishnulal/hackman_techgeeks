package com.ust.product.cigar.report.builder;

import com.ust.product.cigar.api.builder.ReportBuilder;

public abstract class AbstractReportBuilder<FROM,TO,HEADER> implements ReportBuilder<FROM, TO> {

	@Override
	public TO build(FROM from) {
		final HEADER header = buildHeader(from);
		final TO report = buildReport(from,header);
		return report;
	}
	
	protected abstract HEADER buildHeader(FROM from);
	protected abstract TO buildReport(FROM from,HEADER header);

}
