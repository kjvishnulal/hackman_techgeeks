package com.ust.product.cigar.configuration;

import java.lang.reflect.Method;

import org.dozer.spring.DozerBeanMapperFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.web.WebMvcRegistrationsAdapter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.io.Resource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.multipart.support.MultipartFilter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.springframework.web.servlet.view.UrlBasedViewResolver;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesView;

import com.ust.product.cigar.util.constants.BeanIdentifier;
import com.ust.product.cigar.view.common.properties.CommonViewProperties;

import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;

@Configuration
@EntityScan(basePackages = BeanIdentifier.ContextConfiguration.ENTITY_SCAN_PKG)
@EnableJpaRepositories(basePackages = BeanIdentifier.ContextConfiguration.REPOSITORY_SCAN_PKG)
@EnableTransactionManagement
@EnableCaching
@ComponentScan(basePackages = BeanIdentifier.ContextConfiguration.COMPONENT_SCAN_PKG)
@PropertySources(value = { @PropertySource(value = BeanIdentifier.ContextConfiguration.ENV_SPEC_PROPERTY_FILE)})
public class RootConfiguration extends WebMvcConfigurerAdapter {

	@Autowired
	private CommonViewProperties applicationProperties;

	@Bean
	WebMvcRegistrationsAdapter restPrefixAppender() {
		return new WebMvcRegistrationsAdapter() {
			
			@Override
			public RequestMappingHandlerMapping getRequestMappingHandlerMapping() {
				return new RequestMappingHandlerMapping() {
					@Override
					protected RequestMappingInfo getMappingForMethod(Method method, Class<?> handlerType) {
						RequestMappingInfo mappingForMethod = super.getMappingForMethod(method, handlerType);
						if (mappingForMethod != null) {
							return RequestMappingInfo.paths(applicationProperties.getUrlPrefix()).build()
									.combine(mappingForMethod);
						} else {
							return null;
						}
					}
				};
			}
		};
	}

	@Bean
	public CommonsMultipartResolver multipartResolver() {
		return new CommonsMultipartResolver();
	}

	@Bean
	public JRXlsxExporter jrXlsxExporter() {
		return new JRXlsxExporter();
	}

	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename(BeanIdentifier.ContextConfiguration.MESSAGE_SOURCE);
		messageSource.setFallbackToSystemLocale(true);
		return messageSource;
	}

	@Bean
	public DozerBeanMapperFactoryBean dozerBeanMapperFactoryBean(
			@Value(BeanIdentifier.ContextConfiguration.DOZER_MAPPING) Resource[] resources) throws Exception {
		final DozerBeanMapperFactoryBean dozerBeanMapperFactoryBean = new DozerBeanMapperFactoryBean();
		dozerBeanMapperFactoryBean.setMappingFiles(resources);
		return dozerBeanMapperFactoryBean;
	}

	@Bean
	public FilterRegistrationBean multipartFilterRegistrationBean() {
		final MultipartFilter multipartFilter = new MultipartFilter();
		final FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(multipartFilter);
		filterRegistrationBean.addInitParameter("multipartResolverBeanName", "commonsMultipartResolver");
		return filterRegistrationBean;
	}

	@Bean
	public UrlBasedViewResolver viewResolver() {
		UrlBasedViewResolver tilesViewResolver = new UrlBasedViewResolver();
		tilesViewResolver.setViewClass(TilesView.class);
		return tilesViewResolver;
	}

	@Bean
	public TilesConfigurer tilesConfigurer() {
		TilesConfigurer tiles = new TilesConfigurer();
		tiles.setDefinitions(new String[] { BeanIdentifier.ContextConfiguration.TILES_CONFIG });
		return tiles;

	}
	
	

}
