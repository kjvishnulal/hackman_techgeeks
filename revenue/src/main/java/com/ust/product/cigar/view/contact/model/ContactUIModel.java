package com.ust.product.cigar.view.contact.model;

import com.ust.product.cigar.api.model.UIModel;

public class ContactUIModel implements UIModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1018807835728088778L;

	private Integer personalContact;
	private Integer officialContact;

	public Integer getPersonalContact() {
		return personalContact;
	}

	public void setPersonalContact(Integer personalContact) {
		this.personalContact = personalContact;
	}

	public Integer getOfficialContact() {
		return officialContact;
	}

	public void setOfficialContact(Integer officialContact) {
		this.officialContact = officialContact;
	}

}
