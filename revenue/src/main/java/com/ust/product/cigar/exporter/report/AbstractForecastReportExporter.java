package com.ust.product.cigar.exporter.report;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;

import com.ust.product.cigar.api.builder.JasperDatasourceBuilder;
import com.ust.product.cigar.api.exporter.ForecastReportUIExporter;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

public abstract class AbstractForecastReportExporter<Model> implements ForecastReportUIExporter<Model> {

	@Value("${com.ust.revenue.report.jasper.location}")
	private String baseLocation;

	@Autowired
	protected HttpServletResponse servletResponse;

	@Autowired
	protected HttpServletRequest servletRequest;

	@Autowired
	protected ApplicationContext applicationContext;
	
	public void export(Model model) {
		doExport(model, prepareDatasource(model), prepareJasperReport());
	}

	protected JRDataSource prepareDatasource(Model model) {
		return dataSourceBuilder().createDatasource(model);
	}

	protected JasperReport prepareJasperReport() {
		try {
			final File jrxml = applicationContext.getResource(baseLocation + File.separator+ reportName() + ".jrxml").getFile();
			final File jasper = new File(jrxml.getParentFile().getAbsolutePath()+File.separator+ reportName() + ".jasper");

			if (!jasper.exists()) {
				JasperCompileManager.compileReportToFile(jrxml.getAbsolutePath(), jasper.getAbsolutePath());
			}
			JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(jasper.getAbsolutePath());
			return jasperReport;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected abstract void doExport(Model model, JRDataSource filledDatasource,JasperReport report);

	protected abstract String reportName();
	
	protected abstract JasperDatasourceBuilder<Model> dataSourceBuilder();
}
