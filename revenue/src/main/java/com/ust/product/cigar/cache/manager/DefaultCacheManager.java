package com.ust.product.cigar.cache.manager;

import java.util.concurrent.TimeUnit;

import javax.cache.CacheManager;
import javax.cache.configuration.MutableConfiguration;
import javax.cache.expiry.Duration;
import javax.cache.expiry.TouchedExpiryPolicy;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.stereotype.Component;

import com.ust.product.cigar.api.manager.ApplicationCacheManager;

@Component
public class DefaultCacheManager implements JCacheManagerCustomizer, ApplicationCacheManager {

	@Override
	public void customize(CacheManager cacheManager) {
		cacheManager.createCache(DEFAULT_CACHE,
				new MutableConfiguration<>()
						.setExpiryPolicyFactory(TouchedExpiryPolicy.factoryOf(new Duration(TimeUnit.SECONDS, 10)))
						.setStoreByValue(false).setStatisticsEnabled(true));
	}

}
