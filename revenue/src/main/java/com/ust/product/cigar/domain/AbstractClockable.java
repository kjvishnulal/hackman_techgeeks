package com.ust.product.cigar.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@MappedSuperclass
public class AbstractClockable extends AbstractPersistable {
	@Temporal(TemporalType.DATE)
	@Column(name = "Applicable_From")
	private Date applicableFrom;

	@Temporal(TemporalType.DATE)
	@Column(name = "Applicable_Till")
	private Date applicableTill;
}
