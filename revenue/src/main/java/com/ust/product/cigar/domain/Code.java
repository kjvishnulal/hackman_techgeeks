package com.ust.product.cigar.domain;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_CODE")
public class Code extends AbstractPersistable {

	@Column(name = "code")
	private Long code;

	@OneToMany(mappedBy = "code")
	private Collection<CodeDescription> descriptions;

	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public Collection<CodeDescription> getDescriptions() {
		return descriptions;
	}

	public void setDescriptions(Collection<CodeDescription> descriptions) {
		this.descriptions = descriptions;
	}

}
