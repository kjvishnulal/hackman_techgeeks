package com.ust.product.cigar.builder;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.ust.product.cigar.api.builder.JasperDatasourceBuilder;
import com.ust.product.cigar.model.report.GenericForecastReport;
import com.ust.product.cigar.model.report.LocationWiseGenericForecastReportModule;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Component
public class GenericForecastReportDatasourceBuilder implements JasperDatasourceBuilder<GenericForecastReport> {

	private final static DateFormat DFMT = new SimpleDateFormat("dd-MMM-yyyy");
	
	@Override
	public JRDataSource createDatasource(GenericForecastReport model) {
		return new JRBeanCollectionDataSource(getDataBeanList());
	}
	
	public static ArrayList<LocationWiseGenericForecastReportModule> getDataBeanList() {
		ArrayList<LocationWiseGenericForecastReportModule> data = new ArrayList<LocationWiseGenericForecastReportModule>();

		data.add(produce("Cochin", DFMT.format(new Date())));
		data.add(produce("TVM", DFMT.format(new Date())));
		data.add(produce("Chennai", DFMT.format(new Date())));

		return data;
	}

	/**
	 * This method returns a DataBean object, with name and country set in it.
	 */
	private static LocationWiseGenericForecastReportModule produce(String location, String reportDate) {
		LocationWiseGenericForecastReportModule bean = new LocationWiseGenericForecastReportModule();
		bean.setLocation(location);
		bean.setReportDate(reportDate);

		return bean;
	}



}
