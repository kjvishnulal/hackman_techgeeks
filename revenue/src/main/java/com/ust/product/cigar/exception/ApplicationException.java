package com.ust.product.cigar.exception;

public class ApplicationException extends RuntimeException {

	private static final long serialVersionUID = 1876736948480869753L;
	
	public ApplicationException(Exception ex) {
		super(ex);
	}

	public ApplicationException(String message) {
		super(message);
	}
}
