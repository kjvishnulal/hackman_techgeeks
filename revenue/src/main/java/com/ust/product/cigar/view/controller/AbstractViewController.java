package com.ust.product.cigar.view.controller;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.servlet.ModelAndView;

import com.ust.product.cigar.exception.BusinessException;
import com.ust.product.cigar.logger.AbstractApplicationLogger;
import com.ust.product.cigar.view.common.properties.CommonViewProperties;
import com.ust.product.cigar.view.common.properties.WebUIProperties;

@ControllerAdvice
public abstract class AbstractViewController<T> extends AbstractApplicationLogger {

	protected final WebUIProperties uiProperties;
	protected static final String VIEW_PREFIX = "/view";

	@Autowired
	protected CommonViewProperties applicationProperties;

	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addCustomFormatter(new DateFormatter("yyyy-MM-dd"));
	}

	protected AbstractViewController(final WebUIProperties uiProperties) {
		this.uiProperties = uiProperties;
	}
	
	@ExceptionHandler({ BusinessException.class, Exception.class })
	public ModelAndView navigateToErrorUI(HttpServletRequest req, Exception ex) {

		LOG.error("Request: " + req.getRequestURL() + " raised " + ex);

		ModelAndView mav = new ModelAndView();
		mav.addObject("exception", ex);
		mav.addObject("url", req.getRequestURL());
		mav.setViewName(applicationProperties.getErrorUrl());
		return mav;
	}

	protected WebUIProperties getUiProperties() {
		return uiProperties;
	}

	@PostConstruct
	protected abstract void doOnPostConstruction();
//	protected abstract T initializeUICriteria();

}
