package com.ust.product.cigar.exception;

public class BusinessException extends RuntimeException {

	private static final long serialVersionUID = 1876736948480869753L;
	
	public BusinessException(Exception ex) {
		super(ex);
	}

	public BusinessException(String message) {
		super(message);
	}
}
