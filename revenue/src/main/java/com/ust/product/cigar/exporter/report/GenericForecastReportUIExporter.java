package com.ust.product.cigar.exporter.report;

import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.ust.product.cigar.api.builder.JasperDatasourceBuilder;
import com.ust.product.cigar.api.provider.ReportConfigurationProvider;
import com.ust.product.cigar.model.report.GenericForecastReport;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.ReportExportConfiguration;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.XlsxReportConfiguration;

/**
 * API which can be used to render the report into response stream. Controller
 * is the preferred API which should invoke the Exporter because Controller
 * decides the algorithm / approach for report rendering.
 * 
 * Please ensure that every exporter extends <code>AbstractForecastReportExporter</code>
 * 
 * @author UST Global Inc.
 *
 */
@Component
public class GenericForecastReportUIExporter extends AbstractForecastReportExporter<GenericForecastReport> {

	@Value("${com.ust.revenue.report.jasper.name.generic_forecast}")
	private String reportName;

	@Value("${com.ust.revenue.report.xls.name.generic_forecast}")
	private String xlsName;

	@Autowired
	@Qualifier("genericForecastReportConfigurationProvider")
	private ReportConfigurationProvider<ReportExportConfiguration> reportConfigurationProvider;

	@Autowired
	JasperDatasourceBuilder<GenericForecastReport> jasperDatasourceBuilder;
	
	@Autowired
	private JRXlsxExporter xlsExporter;

	@Override
	protected void doExport(GenericForecastReport model, JRDataSource filledDatasource, JasperReport jReport) {
		try {
			final JasperPrint report = JasperFillManager.fillReport(jReport, null, filledDatasource);

			servletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			servletResponse.setHeader("Content-disposition", "attachment; filename=" + prepareAttachmentName());

			final OutputStream outStream = servletResponse.getOutputStream();
			final SimpleExporterInput ip = new SimpleExporterInput(report);
			final XlsxReportConfiguration reportConfiguration = (XlsxReportConfiguration) reportConfigurationProvider.configuration();

			xlsExporter.setExporterInput(ip);
			xlsExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outStream));
			xlsExporter.setConfiguration(reportConfiguration);

			xlsExporter.exportReport();

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	private String prepareAttachmentName() {
		final StringBuilder attachmentNameBuilder = new StringBuilder(xlsName);

		final Date date = Calendar.getInstance().getTime();
		final DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");

		attachmentNameBuilder.append("-").append(dateFormat.format(date)).append(".xlsx");

		return attachmentNameBuilder.toString();
	}

	@Override
	protected String reportName() {
		return reportName;
	}


	@Override
	protected JasperDatasourceBuilder<GenericForecastReport> dataSourceBuilder() {
		return jasperDatasourceBuilder;
	}

}
