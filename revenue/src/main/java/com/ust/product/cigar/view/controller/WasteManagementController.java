package com.ust.product.cigar.view.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ust.product.cigar.api.consumer.service.ConsumerService;
import com.ust.product.cigar.util.constants.BeanIdentifier;
import com.ust.product.cigar.view.consumer.model.ConsumerUIModel;
import com.ust.product.cigar.view.wastemanagement.model.WasteManagementUIModel;
import com.ust.product.cigar.view.wastemanagement.properties.WasteManagementUIProperties;

@Controller(BeanIdentifier.WasteManagementGroup.UI_CONTROLLER_IDENTIFIER)
@RequestMapping("/wasteManagement")
public class WasteManagementController extends UserController<WasteManagementUIProperties> {

	protected final static String VIEW_CRITERIA_MODEL = "consumer-view-criteria";
	protected final static String VIEW_CONSUMER_MODEL = "consumer-view-model";

@Autowired
private ConsumerService consumerService;
	@Autowired
	public WasteManagementController(
			@Qualifier(BeanIdentifier.WasteManagementGroup.UI_PROPERTIES_IDENTIFIER) WasteManagementUIProperties uiProperties) {
		super(uiProperties);
	}

	@Override
	protected void doOnPostConstruction() {
	}


	@ModelAttribute(name = VIEW_CONSUMER_MODEL)
	protected WasteManagementUIModel initializeUIModel() {
		return new WasteManagementUIModel();
	}
	
	public String navigateToLandingPage() {
		return ((WasteManagementUIProperties)uiProperties).getNavigation().getLandingPage();
	}
	
	@PostMapping("register")
	public WasteManagementUIModel save(@ModelAttribute(name = VIEW_CONSUMER_MODEL) WasteManagementUIModel consumerUIRecord) {
		return consumerUIRecord;
	}
	
}
