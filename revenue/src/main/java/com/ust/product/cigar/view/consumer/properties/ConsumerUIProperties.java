package com.ust.product.cigar.view.consumer.properties;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.ust.product.cigar.util.constants.BeanIdentifier;
import com.ust.product.cigar.view.common.properties.NavigationProperties;
import com.ust.product.cigar.view.common.properties.WebUIProperties;


@Component(BeanIdentifier.Consumer.UI_PROPERTIES_IDENTIFIER)
@ConfigurationProperties(prefix = "com.ust.revenue.view.consumer")
public class ConsumerUIProperties implements WebUIProperties {

	@Valid
	@NotNull
	private NavigationProperties navigation;

	public NavigationProperties getNavigation() {
		return navigation;
	}

	public void setNavigation(NavigationProperties navigation) {
		this.navigation = navigation;
	}
	

}
