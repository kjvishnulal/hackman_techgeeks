package com.ust.product.cigar.domain.contact;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.ust.product.cigar.domain.AbstractPersistable;
import com.ust.product.cigar.view.contact.model.ContactUIModel;

@Entity
@Table(name="TBL_CONTACT")
public class Contact extends AbstractPersistable {

	private Integer personalContact;
	private Integer officialContact;

	public Integer getPersonalContact() {
		return personalContact;
	}

	public void setPersonalContact(Integer personalContact) {
		this.personalContact = personalContact;
	}

	public Integer getOfficialContact() {
		return officialContact;
	}

	public void setOfficialContact(Integer officialContact) {
		this.officialContact = officialContact;
	}
	
	public static Contact toContact(ContactUIModel contactUIModel, Contact contact) {
		contact.personalContact = contactUIModel.getPersonalContact();
		contact.officialContact = contactUIModel.getOfficialContact();
		return contact;
	}
}
