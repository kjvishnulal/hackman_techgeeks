package com.ust.product.cigar.api.builder;

public interface ReportBuilder<FROM, TO> {

	TO build(FROM input);
}
