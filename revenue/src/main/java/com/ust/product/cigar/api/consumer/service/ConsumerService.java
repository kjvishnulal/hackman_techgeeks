package com.ust.product.cigar.api.consumer.service;

import com.ust.product.cigar.api.service.CRUDService;
import com.ust.product.cigar.domain.user.Consumer;
import com.ust.product.cigar.view.consumer.model.ConsumerUIModel;

public interface ConsumerService extends CRUDService<Consumer, Long> {

	void save(ConsumerUIModel consumer);
}
