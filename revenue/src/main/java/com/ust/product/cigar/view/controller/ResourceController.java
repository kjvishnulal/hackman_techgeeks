package com.ust.product.cigar.view.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ust.product.cigar.api.exporter.ForecastReportUIExporter;
import com.ust.product.cigar.model.report.GenericForecastReport;
import com.ust.product.cigar.util.constants.BeanIdentifier;
import com.ust.product.cigar.view.consumer.properties.ConsumerUIProperties;

@Controller(BeanIdentifier.Resource.UI_CONTROLLER_IDENTIFIER)
@RequestMapping
public class ResourceController extends AbstractViewController<String> {

	@Autowired
	private ForecastReportUIExporter<GenericForecastReport> genericForecastReportUIExporter;
	
//	@Qualifier(BeanIdentifier.Project.SERVICE_IDENTIFIER) ProjectService<ProjectUICriteria, ProjectUIModel> projectService;
	
	@Autowired
	public ResourceController(@Qualifier(BeanIdentifier.Consumer.UI_PROPERTIES_IDENTIFIER) ConsumerUIProperties homeUiproperties) {
		super(homeUiproperties);
	}
	
	public String navigateToHome() {
		return ((ConsumerUIProperties) uiProperties).getNavigation().getLandingPage();
	}
	
//	@GetMapping
	public String navigateToResourceRegistrationView(HttpServletResponse servletResponse) throws IOException {
		genericForecastReportUIExporter.export(null);
//		projectService.find(null);
		return null;
	}


	@Override
	protected void doOnPostConstruction() {
		// TODO Auto-generated method stub
		
	}
}
