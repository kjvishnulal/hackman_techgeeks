package com.ust.product.cigar.domain.converter;

import javax.persistence.AttributeConverter;

import com.ust.product.cigar.domain.bucket.BucketCategory;

public class BucketCategoryConverter implements AttributeConverter<BucketCategory, String> {

	@Override
	public String convertToDatabaseColumn(BucketCategory bucketCategory) {
		return bucketCategory.getCategory();
	}

	@Override
	public BucketCategory convertToEntityAttribute(String bucketCategory) {
		return BucketCategory.resolveBucketCategory(bucketCategory);
	}

}
