package com.ust.product.cigar.domain.transport;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DriverMappedToVehiclePrimaryKey implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7672990411426142598L;

	@Column(name="VEHICLE_ID")
	private Long vehicleId;
	
	@Column(name="DRIVER_ID")
	private Long driverId;

	public Long getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(Long vehicleId) {
		this.vehicleId = vehicleId;
	}

	public Long getDriverId() {
		return driverId;
	}

	public void setDriverId(Long driverId) {
		this.driverId = driverId;
	}

	
	
}
