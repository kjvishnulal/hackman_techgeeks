package com.ust.product.cigar.domain.user;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.ust.product.cigar.domain.bucket.Bucket;
import com.ust.product.cigar.domain.notification.BucketReadyNotification;

@Entity
@Table(name = "TBL_CONSUMER")
public class Consumer extends AbstractUser {

	@OneToMany(mappedBy = "consumer")
	private Collection<Bucket> buckets;

	@OneToMany(mappedBy = "consumer")
	private Collection<BucketReadyNotification> bids;

	public Collection<Bucket> getBuckets() {
		return buckets;
	}

	public void setBuckets(Collection<Bucket> buckets) {
		this.buckets = buckets;
	}

	public Collection<BucketReadyNotification> getBids() {
		return bids;
	}

	public void setBids(Collection<BucketReadyNotification> bids) {
		this.bids = bids;
	}

}
