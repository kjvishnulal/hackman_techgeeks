package com.ust.product.cigar.util.constants;

public class BeanIdentifier {

	public static class ContextConfiguration {
		private static final String CONF_PREF ="classpath:configuration";
		public static final String ENTITY_SCAN_PKG = "com.ust.product.cigar.domain";
		public static final String REPOSITORY_SCAN_PKG = "com.ust.product.cigar.api.repository";
		public static final String COMPONENT_SCAN_PKG = "com.ust.product.cigar";
		public static final String ENV_SPEC_PROPERTY_FILE = "classpath:configuration/externalization/${env}.properties";
		public static final String CLIENT_PROPERTY_FILE = "classpath:configuration/client/client.properties";
		public static final String MESSAGE_SOURCE = CONF_PREF+"/externalization/messages";
		public static final String DOZER_MAPPING = CONF_PREF+"/dozer/*mappings.xml";
		public static final String TILES_CONFIG = CONF_PREF+"/view/tiles-config.xml";
	}

	public static class Application {
		public static final String CACHE_PROPERTIES_IDENTIFIER = "applicationCacheProperties";
		public static final String LOCATION_SERVICE_IDENTIFIER = "locationService";
		public static final String SERVICE_PROPERTIES_IDENTIFIER = "serviceProperties";
		public static final String UI_PROPERTIES_IDENTIFIER = "uiCommonProperties";
	}

	public static class Consumer {
		public static final String UI_PROPERTIES_IDENTIFIER = "uiConsumerProperties";
		public static final String SERVICE_IDENTIFIER = "consumerService";
		public static final String UI_CONTROLLER_IDENTIFIER = "consumerViewController";
	}
	
	public static class Carrier {
		public static final String UI_PROPERTIES_IDENTIFIER = "uiCarrierProperties";
		public static final String SERVICE_IDENTIFIER = "carrierService";
		public static final String UI_CONTROLLER_IDENTIFIER = "carrierViewController";
	}
	
	public static class Vendor {
		public static final String UI_PROPERTIES_IDENTIFIER = "uiVendorProperties";
		public static final String SERVICE_IDENTIFIER = "vendorService";
		public static final String UI_CONTROLLER_IDENTIFIER = "vendorViewController";
	}
	
	public static class WasteManagementGroup {
		public static final String UI_PROPERTIES_IDENTIFIER = "uiWasteManagementProperties";
		public static final String SERVICE_IDENTIFIER = "wasteManagementService";
		public static final String UI_CONTROLLER_IDENTIFIER = "wasteManagementViewController";
	}

	public static class Resource {
		public static final String UI_CONTROLLER_IDENTIFIER = "resourceViewController";
	}
}
