package com.ust.product.cigar.domain.user;

public enum UserType {

	PUBLIC("1"),
	CARRIER("2"),
	VENDOR("3"),
	WASTEMANAGEMENTGROUP("4");
	
	private String userType;
	
	UserType(String userType) {
		this.userType = userType;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}
	
	public static UserType resolveuserType(String userType) {
		for(final UserType childUserType : UserType.values()) {
			if(childUserType.userType.equals(userType)) {
				return childUserType;
			}
		}
		throw new IllegalArgumentException(userType+" is not a supported user type");
	}
}
