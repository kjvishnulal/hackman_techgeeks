package com.ust.product.cigar.view.carrier.model;

import java.util.Collection;

import javax.validation.constraints.NotNull;

import com.ust.product.cigar.api.model.UIModel;
import com.ust.product.cigar.view.address.model.AddressUIModel;
import com.ust.product.cigar.view.contact.model.ContactUIModel;


public class CarrierUIModel implements UIModel {

	private static final long serialVersionUID = 2502300615974565901L;

	@NotNull
	protected String name;

	protected Long id;
	
	private Collection<AddressUIModel> addresses;
	
	private Collection<ContactUIModel> contacts;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Collection<AddressUIModel> getAddresses() {
		return addresses;
	}

	public void setAddresses(Collection<AddressUIModel> addresses) {
		this.addresses = addresses;
	}

	public Collection<ContactUIModel> getContacts() {
		return contacts;
	}

	public void setContacts(Collection<ContactUIModel> contacts) {
		this.contacts = contacts;
	}
	
	
}
