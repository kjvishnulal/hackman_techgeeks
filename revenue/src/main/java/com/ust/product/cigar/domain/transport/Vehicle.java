package com.ust.product.cigar.domain.transport;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.ust.product.cigar.domain.AbstractPersistable;

@Entity
@Table(name="TBL_VEHICLE")
public class Vehicle extends AbstractPersistable {
	
	@OneToMany
	@JoinTable(name="TBL_DRIVER_VEHICLE_TRIP", joinColumns = {
			@JoinColumn(name="VEHICLE_ID", referencedColumnName="ID")
	})
private Collection<DriverVehicleAssociation> driverMappedForTrips;

}
