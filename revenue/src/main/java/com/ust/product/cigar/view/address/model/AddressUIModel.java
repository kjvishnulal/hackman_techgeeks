package com.ust.product.cigar.view.address.model;

import com.ust.product.cigar.api.model.UIModel;

public class AddressUIModel implements UIModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1018807835728088778L;

	private String houseName;
	private String street;
	private String nearBy;
	private Integer pin;

	public AddressUIModel() {
	}

	public AddressUIModel(String houseName, String street, String nearBy, Integer pin) {
		this.houseName = houseName;
		this.street = street;
		this.nearBy = nearBy;
		this.pin = pin;
	}

	public String getHouseName() {
		return houseName;
	}

	public void setHouseName(String houseName) {
		this.houseName = houseName;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getNearBy() {
		return nearBy;
	}

	public void setNearBy(String nearBy) {
		this.nearBy = nearBy;
	}

	public Integer getPin() {
		return pin;
	}

	public void setPin(Integer pin) {
		this.pin = pin;
	}

}
