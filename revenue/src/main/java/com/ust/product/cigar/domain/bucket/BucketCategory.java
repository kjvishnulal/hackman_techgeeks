package com.ust.product.cigar.domain.bucket;

public enum BucketCategory {

	RED("1"), BLUE("2"), GREEN("3");
	
	private String category;
	
	BucketCategory(String category) {
		this.category = category;
	}

	public static BucketCategory resolveBucketCategory(String bucketCategory) {
		for(final BucketCategory childBucketCategory : BucketCategory.values()) {
			if(childBucketCategory.category.equals(bucketCategory)) {
				return childBucketCategory;
			}
		}
		throw new IllegalArgumentException(bucketCategory+" is not a supported Bucket Category");
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
}
