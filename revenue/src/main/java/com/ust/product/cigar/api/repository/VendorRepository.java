package com.ust.product.cigar.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ust.product.cigar.domain.user.Vendor;

@Repository
public interface VendorRepository extends JpaRepository<Vendor, Long> {

}
