package com.ust.product.cigar.domain.transport;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ust.product.cigar.domain.AbstractPersistable;
import com.ust.product.cigar.domain.user.Consumer;

@Entity
@Table(name = "TBL_CONSUMER_TRANSPORT")
public class ConsumerTransport extends AbstractPersistable {

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "VEHICLE_ID", referencedColumnName = "VEHICLE_ID"),
			@JoinColumn(name = "DRIVER_ID", referencedColumnName = "DRIVER_ID") })
	private DriverVehicleAssociation driverVehicleAssociation;

	@ManyToOne
	@JoinColumn(name = "CONSUMER_ID", referencedColumnName = "ID")
	private Consumer consumer;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "EXPECTED_TIME_FOR_ARRIVAL")
	private Date expectedTimeForArrival;

	@Column(name = "rate")
	private Long rate;

	@Column(name = "confirmed")
	private Boolean confirmed = Boolean.FALSE;

	public DriverVehicleAssociation getDriverVehicleAssociation() {
		return driverVehicleAssociation;
	}

	public void setDriverVehicleAssociation(DriverVehicleAssociation driverVehicleAssociation) {
		this.driverVehicleAssociation = driverVehicleAssociation;
	}

	public Consumer getConsumer() {
		return consumer;
	}

	public void setConsumer(Consumer consumer) {
		this.consumer = consumer;
	}

	public Date getExpectedTimeForArrival() {
		return expectedTimeForArrival;
	}

	public void setExpectedTimeForArrival(Date expectedTimeForArrival) {
		this.expectedTimeForArrival = expectedTimeForArrival;
	}

	public Long getRate() {
		return rate;
	}

	public void setRate(Long rate) {
		this.rate = rate;
	}

	public Boolean getConfirmed() {
		return confirmed;
	}

	public void setConfirmed(Boolean confirmed) {
		this.confirmed = confirmed;
	}

}
