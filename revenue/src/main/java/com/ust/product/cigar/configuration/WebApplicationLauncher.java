package com.ust.product.cigar.configuration;

import java.util.Properties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;
import org.springframework.web.WebApplicationInitializer;



@SpringBootApplication
@Import(RootConfiguration.class)
public class WebApplicationLauncher  extends SpringBootServletInitializer implements WebApplicationInitializer {

	public static void main(String[] args) {
		SpringApplication.run(WebApplicationLauncher.class, args);
		
	}
	
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {		
			Properties properties = new Properties();			
			properties.setProperty("spring.resources.staticLocations","classpath:/res/images/");
			application.application().setDefaultProperties(properties);

        return application.sources(WebApplicationLauncher.class);
    }

	

}