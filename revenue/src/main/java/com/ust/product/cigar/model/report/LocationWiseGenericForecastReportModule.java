package com.ust.product.cigar.model.report;

public class LocationWiseGenericForecastReportModule {

	private String location;

	private String reportDate;

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getReportDate() {
		return reportDate;
	}

	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}

}
