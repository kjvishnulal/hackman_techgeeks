package com.ust.product.cigar.provider.report;

import org.springframework.stereotype.Component;

import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import net.sf.jasperreports.export.XlsxReportConfiguration;

@Component("genericForecastReportConfigurationProvider")
public class GenericForecastReportConfigurationProvider extends AbstractJasperReportConfigurationProvider<XlsxReportConfiguration> {

	@Override
	public XlsxReportConfiguration configuration() {
		  final SimpleXlsxReportConfiguration reportConfiguration = new SimpleXlsxReportConfiguration();
	       reportConfiguration.setRemoveEmptySpaceBetweenColumns(true);
	       reportConfiguration.setRemoveEmptySpaceBetweenRows(true);
	       reportConfiguration.setIgnoreCellBorder(false);
	       reportConfiguration.setShowGridLines(true);
	       reportConfiguration.setWhitePageBackground(false);
	       reportConfiguration.setSheetNames(new String[]{"Generic Forecast"});
		return reportConfiguration;
	}

}
