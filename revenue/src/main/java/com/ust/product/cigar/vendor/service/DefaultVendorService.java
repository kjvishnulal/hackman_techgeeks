package com.ust.product.cigar.vendor.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ust.product.cigar.api.repository.VendorRepository;
import com.ust.product.cigar.api.vendor.service.VendorService;
import com.ust.product.cigar.domain.address.Address;
import com.ust.product.cigar.domain.contact.Contact;
import com.ust.product.cigar.domain.user.UserType;
import com.ust.product.cigar.domain.user.Vendor;
import com.ust.product.cigar.service.AbstractBusinessService;
import com.ust.product.cigar.service.properties.ServiceProperties;
import com.ust.product.cigar.view.address.model.AddressUIModel;
import com.ust.product.cigar.view.contact.model.ContactUIModel;
import com.ust.product.cigar.view.vendor.model.VendorUIModel;

@Service
public class DefaultVendorService extends AbstractBusinessService<Vendor, Long> implements VendorService {

	protected DefaultVendorService(@Autowired ServiceProperties serviceProperties,
			@Autowired VendorRepository consumerRepository) {
		super(serviceProperties, consumerRepository);
	}

	@Override
	public void save(VendorUIModel vendorUIModel) {
		final Vendor vendor = new Vendor();
		vendor.setName(vendorUIModel.getName());
		vendor.setUserType(UserType.VENDOR);
		
		vendor.setAddresses(new ArrayList<Address>());
		for (final AddressUIModel addressUiModel : vendorUIModel.getAddresses()) {
			final Address address = Address.toAddress(addressUiModel, new Address());
			vendor.getAddresses().add(address);
		}

		vendor.setContacts(new ArrayList<Contact>());
		for (final ContactUIModel contactUIModel : vendorUIModel.getContacts()) {
			final Contact contact = Contact.toContact(contactUIModel, new Contact());
			vendor.getContacts().add(contact);
		}
		save(vendor);
	}

}
