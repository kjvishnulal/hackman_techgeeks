package com.ust.product.cigar.domain.bucket;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ust.product.cigar.domain.AbstractPersistable;
import com.ust.product.cigar.domain.converter.BucketCategoryConverter;
import com.ust.product.cigar.domain.user.Consumer;

@Entity
@Table(name = "TBL_BUCKET")
public class Bucket extends AbstractPersistable {

	@ManyToOne
	@JoinColumn(name = "CONSUMER_ID")
	private Consumer consumer;

	@Enumerated
	@Convert(converter = BucketCategoryConverter.class)
	private BucketCategory category;

	@Column(name = "identifier", unique = true)
	private Long identifier;

	public Consumer getConsumer() {
		return consumer;
	}

	public void setConsumer(Consumer consumer) {
		this.consumer = consumer;
	}

	public BucketCategory getCategory() {
		return category;
	}

	public void setCategory(BucketCategory category) {
		this.category = category;
	}

	public Long getIdentifier() {
		return identifier;
	}

	public void setIdentifier(Long identifier) {
		this.identifier = identifier;
	}

}
