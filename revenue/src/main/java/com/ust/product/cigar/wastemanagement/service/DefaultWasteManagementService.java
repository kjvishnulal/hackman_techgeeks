package com.ust.product.cigar.wastemanagement.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ust.product.cigar.api.repository.WasteManagementGroupRepository;
import com.ust.product.cigar.api.wastemgmt.service.WasteManagementGroupService;
import com.ust.product.cigar.domain.address.Address;
import com.ust.product.cigar.domain.contact.Contact;
import com.ust.product.cigar.domain.user.UserType;
import com.ust.product.cigar.domain.user.WasteManagementGroup;
import com.ust.product.cigar.service.AbstractBusinessService;
import com.ust.product.cigar.service.properties.ServiceProperties;
import com.ust.product.cigar.view.address.model.AddressUIModel;
import com.ust.product.cigar.view.contact.model.ContactUIModel;
import com.ust.product.cigar.view.wastemanagement.model.WasteManagementUIModel;

@Service
public class DefaultWasteManagementService extends AbstractBusinessService<WasteManagementGroup, Long> implements WasteManagementGroupService {

	protected DefaultWasteManagementService(@Autowired ServiceProperties serviceProperties,
			@Autowired WasteManagementGroupRepository wasteManagementGroupRepository) {
		super(serviceProperties, wasteManagementGroupRepository);
	}

	@Override
	public void save(WasteManagementUIModel wasteManagementUIModel) {
		final WasteManagementGroup wastemgmt = new WasteManagementGroup();
		wastemgmt.setName(wasteManagementUIModel.getName());
		wastemgmt.setUserType(UserType.WASTEMANAGEMENTGROUP);
		
		wastemgmt.setAddresses(new ArrayList<Address>());
		for (final AddressUIModel addressUiModel : wasteManagementUIModel.getAddresses()) {
			final Address address = Address.toAddress(addressUiModel, new Address());
			wastemgmt.getAddresses().add(address);
		}

		wastemgmt.setContacts(new ArrayList<Contact>());
		for (final ContactUIModel contactUIModel : wasteManagementUIModel.getContacts()) {
			final Contact contact = Contact.toContact(contactUIModel, new Contact());
			wastemgmt.getContacts().add(contact);
		}
		save(wastemgmt);
	}

}
