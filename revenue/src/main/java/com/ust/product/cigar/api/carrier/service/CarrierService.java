package com.ust.product.cigar.api.carrier.service;

import com.ust.product.cigar.api.service.CRUDService;
import com.ust.product.cigar.domain.user.Carrier;
import com.ust.product.cigar.view.carrier.model.CarrierUIModel;

public interface CarrierService extends CRUDService<Carrier, Long> {

	void save(CarrierUIModel carrierUIModel);
}
