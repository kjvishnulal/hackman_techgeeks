package com.ust.product.cigar.service.properties;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.ust.product.cigar.util.constants.BeanIdentifier;


@Component(BeanIdentifier.Application.SERVICE_PROPERTIES_IDENTIFIER)
@ConfigurationProperties(prefix = "com.ust.revenue.service")
public class ServiceProperties {

	@Valid
	@NotNull
	private ProjectProperties project;

	public ProjectProperties getProject() {
		return project;
	}

	public void setProject(ProjectProperties project) {
		this.project = project;
	}
	
	
}
