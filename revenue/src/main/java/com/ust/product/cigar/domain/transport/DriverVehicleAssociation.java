package com.ust.product.cigar.domain.transport;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_DRIVER_VEHICLE")
public class DriverVehicleAssociation {

	@EmbeddedId
	private DriverMappedToVehiclePrimaryKey pk;

	public DriverMappedToVehiclePrimaryKey getPk() {
		return pk;
	}

	public void setPk(DriverMappedToVehiclePrimaryKey pk) {
		this.pk = pk;
	}
	


	
}
