<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html lang="en">
<head>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Revenue</title>

<!-- jQuery -->
	<script src="${contextPath}/js/jquery.js"></script>

<script type="text/javascript">
$.noConflict();
jQuery(document).ready(function(){
	jQuery( "#addProjectLink" ).click(function() {
		var length = jQuery("#projects_size").val();
		var currentIndex = 0;
		if(length != '0') {
			currentIndex = length;
		} else {
			jQuery("#projects_size").val(1);
		}
		 
		 var projectDiv = jQuery("#copy_panel").clone();
		 projectDiv.css('visibility','visible')
		 projectDiv.prependTo("#projectColumn");
		 
		 var projectText = projectDiv.children()[1];
		 var costText = projectDiv.children()[3];
		 var replacement = '['.concat(currentIndex).concat(']');
		 
		 var projectName = projectText.name;
		 projectText.name = projectName.replace('[]',replacement);

		 var costName = costText.name;
		 costText.name = costName.replace('[]',replacement);

		 jQuery("</br>").prependTo("#addProjectLink");
	});	
});
</script>
<!-- Bootstrap Core CSS -->
<link href="${contextPath}/css/bootstrap.min.css" rel="stylesheet">
<!-- Custom CSS -->
<!-- Custom Fonts -->
<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet"
	type="text/css">
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700,400italic,700italic"
	rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Montserrat:400,700"
	rel="stylesheet" type="text/css">
	<!-- Bootstrap Core JavaScript -->
	<script src="${contextPath}/js/bootstrap.min.js"></script>
	<!-- Plugin JavaScript -->
	<script src="${contextPath}/js/jquery.easing.min.js"></script>
	<!-- Custom Theme JavaScript -->
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>


<body id="page-top">
	<!-- Navigation -->
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-main-collapse">
				<i class="fa fa-bars"></i>
			</button>
			<a class="navbar-brand page-scroll" href="index.html"> Sempra
				Revenue Forecase </a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
<!-- 		<div -->
<!-- 			class="collapse navbar-collapse navbar-right navbar-main-collapse"> -->
<!-- 			<ul class="nav navbar-nav"> -->
<%-- 				<li><a href="${contextPath}/view/index">Home</a></li> --%>
<!-- 				<li><a href="portfolio-item.html">Resource</a></li> -->
<!-- 				<li><a href="#">SOW</a></li> -->
<!-- 				<li><a href="contact.html">Manage Software</a></li> -->
<!-- 				<li><a href="contact.html">Resource Cost</a></li> -->
<!-- 				<li><a href="contact.html">Vacation / Holidays</a></li> -->
<!-- 				<li><a href="contact.html">Expense</a></li> -->
<!-- 				<li><a href="contact.html">AOP</a></li> -->


<!-- 			</ul> -->
<!-- 		</div> -->
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container --> </nav>
	<!-- Intro Header -->
	<header class="intro">
	<div class="intro-body">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<h2>Save Sow</h2>
					<form:form method="post" modelAttribute="sow" action="${contextPath}/view/sow/save" >
						<div class="rTable">
							<div class="rTableRow">
								<div class="rTableCell">Name of Sow</div>
								<div class="rTableCell">
									<input type="text" name="sow" />
								</div>
							</div>
							<div class="rTableRow">
								<div class="rTableCell">Add Projects</div>
								<div class="rTableCell" id="projectColumn">
									<div id="copy_panel" style="visibility: hidden;"> 
										<label for="projectTextBox">Add Projects</label>	
										<input name="projects[${z}].project" value="${project.project}"/>
										<label>If flat monthly invoicing($)</label>
										<input name="projects[${z}].cost" value="${project.cost}"/>
									</div>
									<input type="hidden" id="projects_size" value="${fn:length(sow.projects)}">
									<c:forEach items="${sow.projects}" var="project" varStatus="status">
										<div> 
											<label for="projectTextBox">Add Projects</label>	
											<input name="projects[${status.index}].project" value="${project.project}"/>
											<label>If flat monthly invoicing($)</label>
											<input name="projects[${status.index}].cost" value="${project.cost}"/>
										</div>
									</c:forEach>
									<a href="#" id="addProjectLink">Add Project</a>
								</div>
							</div>
							
							<div class="rTableRow">
								<div class="rTableCell"><input type="submit" value="Save"/></div>
							</div>
						</div>
					</form:form>
				</div>
			</div>
		</div>
	</div>
	</header>

	<!-- Footer -->
	<footer>
	<div></div>
	</footer>
</body>
</html>