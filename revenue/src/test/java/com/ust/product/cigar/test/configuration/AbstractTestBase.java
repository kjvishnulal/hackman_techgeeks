package com.ust.product.cigar.test.configuration;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ust.product.cigar.configuration.RootConfiguration;


@RunWith(SpringRunner.class)
@SpringBootTest(classes={RootConfiguration.class})
public abstract class AbstractTestBase {

}
